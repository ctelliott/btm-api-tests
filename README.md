# BTM API Tests

## Generating the comparison results



###Install dependencies (NodeJS required)
```
npm install
```

Ensure BTM Test is pointed at Upgrade (atasker@liberty.edu can do this for you)

###Run the script
```
npm run test
```

Results appear as time-stamped json documents in the results directory. There will be one file for prod endpoints, and one for banner upgrade. Take these documents and paste their contents into https://jsonformatter.curiousconcept.com/ and download the formatted results for both files. Then go to https://www.diffchecker.com/ and paste the formatted prod results on the left, upgrade on the right.

The comparison tool will show you any discrepencies in the returned data. If any are found, email ctelliott@liberty.edu or glyoder@liberty.edu with your findings. Happy testing!

Note: Feel free to mess wit the dates at the top of the data.js file to generate meaningful results.
