var request = require('request-promise');
var endpoints = require('./data')
var prod = []
var upgrade = []
var conflict = []
var num = endpoints.length

asyncForEach(endpoints, async (endpoint) => {
    var prodRes = await request({
        method: 'get',
        uri: 'https://www.' + endpoint,
        json: true
    }). then((res) => {
        prod.push({
            endpoint: endpoint,
            res: res
        })
    }).catch(err => {console.log(err)})

    var upgradeReq = await request({
        method: 'get',
        uri: 'https://test.' + endpoint,
        json: true
    }).then((res) => {
        upgrade.push({
            endpoint: endpoint,
            res: res
        })
    }).catch(err => {console.log(err)})

    for (let index = 0; index < prod.length; index++) {
        //remove links as they are always different
        if(prod[index].res.links){
            delete prod[index].res.links
        }
        if(upgrade[index].res.links){
            delete upgrade[index].res.links
        }
        if(Array.isArray(prod[index].res)){
            prod[index]['res'].forEach(el => {
                el['links'] = null
            });
        }
        if(Array.isArray(upgrade[index].res)){
            upgrade[index]['res'].forEach(el => {
                el['links'] = null
            });
        }
        if(upgrade[index].res !== prod[index].res){
            conflict.push({
                prod: prod[index],
                upgrade: upgrade[index]
            })
        }
    }
    writeFiles('upgrade', upgrade)
    writeFiles('prod', prod)
    //writeFiles('conflict', conflict)
})

function writeFiles(name, data){
    if(name == 'conflict'){
        var fs = require("fs");
        fs.writeFile(`./results/${Date.now()}_${name}.json`, JSON.stringify(data), (err) => {
            if (err) {
                console.error(err);
                return;
            };
        }); 
    }
    if(data.length == num){
        var fs = require("fs");
        fs.writeFile(`./results/${Date.now()}_${name}.json`, JSON.stringify(data), (err) => {
            if (err) {
                console.error(err);
                return;
            };
        });
    }
}

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  }