const student = 'ctelliott'
const beforeDate = '2019-01-09'
const afterDate = '2019-01-06'
const asOf = '2019-01-07'
const program = 'mbat-mba-d'
const term = '201920'
const group = 'MED'

module.exports = [
    `liberty.edu/termmgr/terms?withStartDateBefore=${beforeDate}&withStartDateAfter=${afterDate}`, 
    `liberty.edu/termmgr/terms?withStartDateBefore=${beforeDate}`, 
    `liberty.edu/termmgr/terms?withStartDateAfter=${afterDate}`, 
    `liberty.edu/termmgr/terms?inProgress=true&asOf=${asOf}`, 
    `liberty.edu/termmgr/terms?inProgress=true`, 
    `liberty.edu/termmgr/terms?forProgram=${program}&withStartDateBefore=${beforeDate}&withStartDateAfter=${afterDate}`, 
    `liberty.edu/termmgr/terms?forProgram=${program}&withStartDateBefore=${beforeDate}`, 
    `liberty.edu/termmgr/terms?forProgram=${program}&withStartDateAfter=${afterDate}`, 
    `liberty.edu/termmgr/terms?forProgram=${program}`, 
    `liberty.edu/termmgr/terms?forPerson=${student}&withStartDateBefore=${beforeDate}&withStartDateAfter=${afterDate}`, 
    `liberty.edu/termmgr/terms?forPerson=${student}&withStartDateBefore=${beforeDate}`, 
    `liberty.edu/termmgr/terms?forPerson=${student}&withStartDateAfter=${afterDate}`, 
    `liberty.edu/termmgr/terms?forPerson=${student}&whereEnrolled=true&withStartDateBefore=${beforeDate}&withStartDateAfter=${afterDate}`, 
    `liberty.edu/termmgr/terms?forPerson=${student}&whereEnrolled=true&withStartDateBefore=${beforeDate}`, 
    `liberty.edu/termmgr/terms?forPerson=${student}&whereEnrolled=true&withStartDateAfter=${afterDate}`, 
    `liberty.edu/termmgr/terms?forPerson=${student}&whereEnrolled=true&excludingThoseWithNews101=true&withStartDateBefore=${beforeDate}&withStartDateAfter=${afterDate}`, 
    `liberty.edu/termmgr/terms?forPerson=${student}&whereEnrolled=true&excludingThoseWithNews101=true&withStartDateBefore=${beforeDate}`, 
    `liberty.edu/termmgr/terms?forPerson=${student}&whereEnrolled=true&excludingThoseWithNews101=true&withStartDateAfter=${afterDate}`, 
    `liberty.edu/termmgr/terms?forPerson=${student}&whereEnrolled=true&excludingThoseWithNews101=true`, 
    `liberty.edu/termmgr/terms?forPerson=${student}&whereEnrolled=true`, 
    `liberty.edu/termmgr/terms?forPerson=somethingrandomnotastudent123&whereEnrolled=true`, 
    `liberty.edu/termmgr/terms?forPerson=${student}`, 
    `liberty.edu/termmgr/terms?forInstructor=${student}`, 
    `liberty.edu/termmgr/terms/next?after=${term}&forProgram=${program}&excludingMinorTerms=true`, 
    `liberty.edu/termmgr/terms/next?after=${term}&forProgram=${program}`, 
    `liberty.edu/termmgr/terms/next?after=${term}&forPerson=${student}&excludingMinorTerms=true`, 
    `liberty.edu/termmgr/terms/next?after=${term}&forPerson=${student}`, 
    `liberty.edu/termmgr/terms/next?after=${term}`, 
    `liberty.edu/termmgr/terms/in-progress?forProgram=${program}&asOf=${asOf}`, 
    `liberty.edu/termmgr/terms/in-progress?forProgram=${program}`, 
    `liberty.edu/termmgr/terms/in-progress?forPerson=${student}&asOf=${asOf}`, 
    `liberty.edu/termmgr/terms/in-progress?forPerson=${student}`, 
    `liberty.edu/termmgr/terms/in-progress?asOf=${asOf}`, 
    `liberty.edu/termmgr/terms/in-progress`, 
    `liberty.edu/termmgr/terms/current?forProgram=${program}`, 
    `liberty.edu/termmgr/terms/current?forPerson=${student}`, 
    `liberty.edu/termmgr/terms/current`, 
    `liberty.edu/termmgr/terms/${term}/groups`, 
    `liberty.edu/termmgr/terms/${term}`, 
    `liberty.edu/termmgr/terms`, 
    `liberty.edu/termmgr/groups?forProgram=${program}`, 
    `liberty.edu/termmgr/groups/default`, 
    `liberty.edu/termmgr/groups/${group}/terms`, 
    `liberty.edu/termmgr/groups/${group}`, 
    `liberty.edu/termmgr/groups`
]